<?php
/**
 * LogoutAfterOrderPlace
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Observer;


use Magento\Customer\Model\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class LogoutAfterOrderPlace implements ObserverInterface
{
    protected $_session;

    public function __construct(Session $_session)
    {
        $this->_session = $_session;
    }

    public function execute(Observer $observer)
    {
        if ($this->_session->isLoggedIn()) {
            $customerId = $this->_session->getId();
            if ($customerId) {
                $this->_session
                    ->logout();
//                    ->setBeforeAuthUrl($this->redirect->getRefererUrl())
//                    ->setLastCustomerId($customerId);
//                return "Customer logout successfully";
            }
        }
        // TODO: Implement execute() method.
    }
}
