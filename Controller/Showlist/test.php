<?php
/**
 * test
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Controller\Showlist;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magestore\ZeroTraining\Model\FilmFactory;
use Magestore\ZeroTraining\Model\CategoryFactory;

use Magestore\ZeroTraining\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magestore\ZeroTraining\Model\ResourceModel\Film\CollectionFactory as FilmCollectionFactory;
class test extends Action
{
    private $_modelFilmFactory;
    private $_modelCategoryFactory;
    private $_collectionFilmFactory;
    private $_collectionCategoryFactory;

    public function __construct(Context $context,
                                FilmFactory $_modelFilmFactory,
                                FilmCollectionFactory $_collectionFilmFactory,
                                CategoryFactory $_modelCategoryFactory,
                                CategoryCollectionFactory $_collectionCategoryFactory
    )
    {
        parent::__construct($context);
        $this->_modelFilmFactory = $_modelFilmFactory;
        $this->_collectionFilmFactory = $_collectionFilmFactory;
        $this->_modelCategoryFactory = $_modelCategoryFactory;
        $this->_collectionCategoryFactory = $_collectionCategoryFactory;
    }

    public
    function execute()
    {
//        $modelFilm = $this->_modelFilmFactory->create();
        //Calling model
//        $filmModel = $modelFilm->load(2);
//        $collectionFilm = $this->_collectionFilmFactory->create();

        $categoryModel = $this->_modelCategoryFactory->create();
//        var_dump($categoryModel->load(1)->getData());
        $collectionCategory = $this->_collectionCategoryFactory ->create();
//        var_dump($collectionCategory->getSelect()->__toString());
        $rs = $collectionCategory
            ->setOrder('num_of_actor', 'DESC')
            ->setPageSize(5)
            ->setCurPage(1)
            ->getData();
        var_dump($rs);

//        //Calling Collection
//        // 1/ Call by model
//        $filmCollection = $modelFilm->getCollection();
//        // 2/ Call directly

        //Magic method
//        var_dump($filmModel->getListActors());
//        $filmModel->setDatPham('ALO ALO');
//        var_dump($filmModel->getDatPham());
//        $filmModel->setData('dat_pham','alo alo');

//        var_dump($collectionFilm->getSelectCountSql()->__toString());
//        var_dump($collectionFilm->getData());
    }
}
