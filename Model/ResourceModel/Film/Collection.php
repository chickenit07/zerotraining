<?php
/**
 * Collection
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Model\ResourceModel\Film;

use \Magestore\ZeroTraining\Model\Film as Model;
use \Magestore\ZeroTraining\Model\ResourceModel\Film as ResourceModel;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()
            ->reset()
            ->from($this->getMainTable())
            ->joinLeft(['film_category' => $this->getTable('zero_training_four_film_category')],
                'zero_training_four_film.film_id = film_category.film_id',null)
            ->joinLeft(['category' => $this->getTable('zero_training_four_category')],
                'film_category.category_id = category.category_id', null)
            ->joinLeft(['film_actor' => $this->getTable('zero_training_four_film_actor')],
                'zero_training_four_film.film_id = film_actor.film_id',null)
            ->joinLeft(['actor' => $this->getTable('zero_training_four_actor')],
                'actor.actor_id = film_actor.actor_id',null)
            ->columns(
                [
                    'category_name' => 'category.name',
                    'num_of_actor' => 'COUNT(film_actor.actor_id)',
                    'list_actors' => "GROUP_CONCAT(CONCAT(`actor`.first_name,' ',`actor`.last_name))"
                ]
            )
            ->having("num_of_actor > 5")
            ->group('film_actor.film_id');
        var_dump($this->getSelect()->__toString());
        return $this;
    }
}
