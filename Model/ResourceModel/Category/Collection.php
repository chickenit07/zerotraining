<?php
/**
 * Collection
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Model\ResourceModel\Category;


use Magestore\ZeroTraining\Model\Category as Model;
use Magestore\ZeroTraining\Model\ResourceModel\Category as ResourceModel;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()
            ->reset()
            ->from($this->getMainTable())
            ->joinLeft(['film_category' => $this->getTable('zero_training_four_film_category')],
                'zero_training_four_category.category_id = film_category.category_id', null)
            ->joinLeft(['film' => $this->getTable('zero_training_four_film')],
                'film_category.film_id = film.film_id', null)
            ->joinLeft(['film_actor' => $this->getTable('zero_training_four_film_actor')],
                'film_actor.film_id = film.film_id', null)
            ->columns(
                [
                    'num_of_actor' => 'COUNT(film_actor.actor_id)'
                ]
            )->group('film_category.category_id');
        var_dump($this->getSelect()->__toString());
        return $this;
    }
}
