<?php
/**
 * Category
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Model\ResourceModel;


use phpDocumentor\Reflection\Types\This;

class Category extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('zero_training_four_category', 'category_id');
    }

    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        $select
            ->joinLeft(['film_category' => $this->getTable('zero_training_four_film_category')],
                'zero_training_four_category.category_id = film_category.category_id', null)
            ->joinLeft(['film' => $this->getTable('zero_training_four_film')],
                'film_category.film_id = film.film_id', null)
            ->joinLeft(['film_actor' => $this->getTable('zero_training_four_film_actor')],
                'film_actor.film_id = film.film_id', null)
            ->columns(
                [
                    'num_of_actor' => 'COUNT(film_actor.actor_id)'
                ]
            );
//            ->group('film_actor.film_id');
        return $select;
    }
}
