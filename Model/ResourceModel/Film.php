<?php
/**
 * Film
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Model\ResourceModel;


use Magento\Framework\Exception\LocalizedException;
use phpDocumentor\Reflection\Types\This;

class Film extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('zero_training_four_film', 'film_id');
        // TODO: Implement _construct() method.
    }

    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field,$value,$object);
        $select
            ->joinLeft(array('film_category' => $this->getTable('zero_training_four_film_category')),
                'zero_training_four_film.film_id = film_category.film_id',null)
            ->joinLeft(array('category' => $this->getTable('zero_training_four_category')),
                'film_category.category_id = category.category_id', null)
            ->joinLeft(array('film_actor' => $this->getTable('zero_training_four_film_actor')),
                'zero_training_four_film.film_id = film_actor.film_id',null)
            ->joinLeft(array('actor' => $this->getTable('zero_training_four_actor')),
                'actor.actor_id = film_actor.actor_id',null)
            ->columns(
                [
                    'category_name' => 'category.name',
                    'num_of_actor' => 'COUNT(film_actor.actor_id)',
                    'list_actors' => "GROUP_CONCAT(CONCAT(`actor`.first_name,' ',`actor`.last_name))"
                ])
            ->group('film_actor.film_id');
        return $select;
    }


}

