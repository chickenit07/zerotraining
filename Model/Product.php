<?php
/**
 * Product
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Model;


class Product extends \Magento\Catalog\Model\Product
{
    public function getName()
    {
        return $this->_getData('name') . '_checked';
    }
}
