<?php
/**
 * Film
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Model;


use Magestore\ZeroTraining\Model\ResourceModel\Film as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class Film extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
