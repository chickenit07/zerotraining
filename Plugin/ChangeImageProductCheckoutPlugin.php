<?php
/**
 * ChangeImageProductCheckoutPlugin
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Plugin;


class ChangeImageProductCheckoutPlugin
{
    public function afterGetImage($item, $result)
    {
        return $result->setImageUrl("https://cdn.itviec.com/employers/magestore/logo/social/5r69864WiH5MdBM1iFi1788o/magestore-logo.png");
    }

}
