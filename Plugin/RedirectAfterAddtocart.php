<?php
/**
 * RedirectAfterAddtocart
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Plugin;
use Magento\Framework\Controller\ResultFactory;

class RedirectAfterAddtocart
{
    protected $_url;

    protected $request;

    public function __construct(
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\Request\Http $request
    )
    {
        $this->_url = $url;
        $this->request = $request;
    }

    public function aroundExecute(\Magento\Checkout\Controller\Cart\Add $subject, \Closure $proceed)
    {
        $returnValue = $proceed();
        // We need to check, does the request send from Ajax?
        if (!$this->request->isAjax()) {
            // get the url of the checkout page
            $checkoutUrl = $this->_url->getUrl('checkout/cart');
            // set the url for redirecting
            $returnValue->setUrl($checkoutUrl);
        }
        return $returnValue;
    }
//    private $_url;
//    private $request;
//    private $resultFactory;
//    public function __construct(\Magento\Framework\UrlInterface $url,
//                                \Magento\Framework\App\Request\Http $request,
//                                \Magento\Framework\Controller\ResultFactory $resultFactory)
//    {
//        $this->_url= $url;
//        $this->request = $request;
//        $this->$resultFactory = $resultFactory;
//    }
//
//    public function aroundExecute(\Magestore\ZeroTraining\Observer\DiscountOffAfterAddToCart $subject, \Closure $proceed)
//    {
//        $returnValue = $proceed();
//        // We need to check, does the request send from Ajax?
//        if (!$this->request->isAjax()) {
//            // get the url of the checkout page
//            $checkoutUrl = $this->_url->getUrl('checkout/index/index');
//            // set the url for redirecting
//            $returnValue->setUrl($checkoutUrl);
//        }
//        return $returnValue;
//    }
//    public function afterExecute(\Magestore\ZeroTraining\Observer\DiscountOffAfterAddToCart $subject, $result)
//    {
//        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
//        if($this->request->isAjax()){
//            $checkoutUrl = $this->_url->getUrl('checkout/index/index');
//            $resultRedirect->setUrl($checkoutUrl);
//            return $resultRedirect;
//        }
//    }

}
