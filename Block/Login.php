<?php
/**
 * Login
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Block;


class Login extends \Magento\Customer\Block\Form\Login
{
    private $_httpContext;
    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                \Magento\Customer\Model\Session $customerSession,
                                \Magento\Customer\Model\Url $customerUrl,
                                \Magento\Framework\App\Http\Context $_httpContext,
                                array $data = [])
    {
        $this->_httpContext= $_httpContext;
        parent::__construct($context, $customerSession, $customerUrl, $data);
    }

    public function isCustomerLoggedIn()
    {
        return $this->_httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }
}
