<?php
/**
 * CurProductCategoryName
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\ZeroTraining\Block;


use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class CurProductCategoryName extends Template
{
    private $_registry;
    private $_catalogModelProduct;
    private $_catalogModelCategory;

    /**
     * CurProductCategoryName constructor.
     * @param Template\Context $context
     * @param array $data
     * @param \Magento\Catalog\Model\ProductFactory $_catalogModelProduct
     * @param \Magento\Catalog\Model\CategoryFactory $_catalogModelCategory
     */
    public function __construct(Template\Context $context,
                                array $data = [],
                                \Magento\Catalog\Model\ProductFactory $_catalogModelProduct,
                                \Magento\Catalog\Model\CategoryFactory $_catalogModelCategory
                                )
    {
        parent::__construct($context, $data);
        $this->_catalogModelProduct = $_catalogModelProduct;
        $this->_catalogModelCategory = $_catalogModelCategory;
    }

    public function getCurProductCategoryInfo()
    {
        $id = $this->getRequest()->getParam('id');
        $product = $this->_catalogModelProduct->create()->load($id);
        $categoryIds = $product->getCategoryCollection()->getAllIds();

        $category = $this->_catalogModelCategory->create();
        foreach ($categoryIds as $categoryId) {
            $categoryName = $category->load($categoryId)->getName();
            echo $categoryName . '';
        }
        // 1. fetch the product
        // 2. get the category info from the product
        // 3. return it here
    }
}
